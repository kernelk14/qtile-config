# My qtile config!

![Image Screenshot](screen.png)

## What is this?
This is basically my Qtile config.

## Usage
```console
$ git clone https://kernelk14/qtile-config
$ cd qtile-config
$ cp config.py ~/.config/qtile/config.py
```
